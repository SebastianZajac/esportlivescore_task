import angular from 'angular';
import Home from './home/home';
import Game from './game/game'

let componentModule = angular.module('app.components', [
  Home,
  Game
])

.name;

export default componentModule;
