import angular from 'angular';
import gameComponent from './game.component';
import gamePlayerService from './game-player/game-player.service';
import gameBuilderFactory from './game-builder/game-builder.factory';
import spinnerFactory from './game-builder/objects/spinner.factory';

let gameModule = angular.module('game', [])
    .component('game', gameComponent)
    .service('gamePlayerService', gamePlayerService)
    .service('gameBuilderFactory', gameBuilderFactory)
    .service('spinnerFactory', spinnerFactory)
    .name;

export default gameModule;
