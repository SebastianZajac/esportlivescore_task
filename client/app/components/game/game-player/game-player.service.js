export default class GamePlayerService {

    /**
     * {GameBuilderFactory} gameBuilderFactory
     */
    gameBuilderFactory;

    /**
     * @param {GameBuilderFactory} gameBuilderFactory
     */
    constructor(gameBuilderFactory) {
        "ngInject";
        this.slotsNumber = 3;
        this.gameBuilderFactory = gameBuilderFactory;
        this.gameBuilderFactory.initGame(this.slotsNumber);
    }

    startGame () {
        this.gameBuilderFactory.startSpinning();
    }

    stopGame (onWinCallback, onLooseCallback) {
        this.gameBuilderFactory.stopSpinning(
            (result) => {
                let isWinner = this.checkGameResult(result);
                isWinner ? onWinCallback() : onLooseCallback();
            }
        )
    }

    checkGameResult (result) {
        return Object.values(result).indexOf(this.slotsNumber) !== -1;
    }
};


