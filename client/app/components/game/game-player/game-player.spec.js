import GameModule from './../game'

describe('Game player', () => {
    let gameBuilderFactory;

    beforeEach(angular.mock.module(GameModule));

    beforeEach(window.module(($provide) => {
        let gameBuilderFactoryMock = {
            initGame () {}
        };
        $provide.value('gameBuilderFactory', gameBuilderFactoryMock);
    }));

    beforeEach(inject(($injector) => {
        gameBuilderFactory = $injector.get('gameBuilderFactory');
    }));

    describe('Service', () => {
        let service;

        beforeEach(inject(($injector) => {
            service = $injector.get('gamePlayerService');
        }));

        it('should contain slots number defined after initialization', () => {
            expect(service.slotsNumber).to.eq(3);
        });

        it('should start game from game builder', () => {
            let spy = chai.spy.on(gameBuilderFactory, 'startSpinning');
            service.startGame();
            spy.should.have.been.called.exactly(1);
        });

        it('should stop the game', () => {
            let spy = chai.spy.on(gameBuilderFactory, 'stopSpinning');
            service.stopGame();
            spy.should.have.been.called.exactly(1);
        });

        it('should define the winner', () => {
            let value = {1: 3};
            service.slotsNumber = 3;
            expect(service.checkGameResult(value)).to.be.true;
            value = {1: 1, 2: 3};
            expect(service.checkGameResult(value)).to.be.true;
        });

        it('should define the looser', () => {
            let value = {1: 2, 2: 1};
            service.slotsNumber = 3;
            expect(service.checkGameResult(value)).to.be.false;
            value = {1: 4, 2: 4};
            expect(service.checkGameResult(value)).to.be.false;
        });
    });
});
