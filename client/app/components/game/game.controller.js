class GameController {
    /**
     * {GamePlayer} gamePlayerService
     */
    gamePlayerService;

    isWinner;

    /**
     * @param {GamePlayer} gamePlayerService
     * @param $scope
     */
    constructor(gamePlayerService, $scope) {
        "ngInject";
        this.scope = $scope;
        this.gamePlayerService = gamePlayerService;
        this.isWinner = null;
    }

    onWinning () {
        this.scope.$apply(() => (
            this.isWinner = true
        ));
    }

    onLoose () {
        this.scope.$apply(() => (
            this.isWinner = false
        ));
    }

    startGame () {
        this.isWinner = null;
        this.gamePlayerService.startGame();
    }

    stopGame () {
        this.gamePlayerService.stopGame(
            this.onWinning.bind(this),
            this.onLoose.bind(this)
        );
    }

}

export default GameController;
