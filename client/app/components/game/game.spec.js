import GameModule from './game'

describe('Game', () => {
    let $rootScope, $componentController, $compile, gamePlayerService;

    beforeEach(angular.mock.module(GameModule));

    beforeEach(angular.mock.module(($provide) => {
        let gamePlayerServiceMock = {
            stopGame () {},

            startGame () {}
        };
        $provide.value('gamePlayerService', gamePlayerServiceMock);
    }));

    beforeEach(inject(($injector) => {
        $rootScope = $injector.get('$rootScope');
        $componentController = $injector.get('$componentController');
        $compile = $injector.get('$compile');
        gamePlayerService = $injector.get('gamePlayerService');
    }));

    describe('Controller', () => {
        let controller, scope;
        beforeEach(() => {
            scope = $rootScope.$new();
            controller = $componentController('game', {
                gamePlayerService: gamePlayerService,
                $scope: scope,
            });
        });

        it('has a is winner property', () => {
            expect(controller).to.have.property('isWinner');
        });

        it('sets isWinner property to true', () => {
            controller.onWinning();
            expect(controller.isWinner).to.be.true;
        });

        it('sets isWinner property to false', () => {
            controller.onLoose();
            expect(controller.isWinner).to.be.false;
        });

        it('starts game via service and resets isWinner prop', () => {
            let spy = chai.spy.on(gamePlayerService, 'startGame');
            controller.startGame();
            expect(controller.isWinner).to.be.null;
            spy.should.have.been.called.exactly(1);
        });

        it('stops game via service and executes one of callbacks', () => {
            let spy = chai.spy.on(gamePlayerService, 'stopGame');
            controller.stopGame();
            expect(controller.isWinner).to.be.null;
            spy.should.have.been.called.exactly(1);
        });
    });

    describe('View', () => {
        let scope, template, element;

        beforeEach(() => {
            scope = $rootScope.$new();
            element = angular.element('<game></game>');
            template = $compile(element)(scope);
            scope.$apply();
        });

        it('has #id game', () => {
            expect(template.html().indexOf('game-window') !== -1).to.be.true;
        });

        it('has start button', () => {
            expect(template.html().indexOf('button-start') !== -1).to.be.true;
        });

        it('has stop button', () => {
            expect(template.html().indexOf('button-stop') !== -1).to.be.true;
        });

        it('has no result message when default state', () => {
            expect(template.html().indexOf('winner-message') === -1).to.be.true;
            expect(template.html().indexOf('looser-message') === -1).to.be.true;
        });
    });
});
