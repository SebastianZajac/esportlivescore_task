import {
    Object3D,
    CylinderGeometry,
    MeshBasicMaterial,
    Mesh,
    RepeatWrapping,
    TextureLoader,
    MeshFaceMaterial,
    Texture
} from 'three';

export default class Spinner extends Object3D {
    imgArray = [
        '../../assets/img/21.jpg',
        '../../assets/img/22.jpg',
        '../../assets/img/23.jpg',
        '../../assets/img/24.jpg',
        '../../assets/img/25.jpg',
        '../../assets/img/26.jpg',
        '../../assets/img/27.jpg',
        '../../assets/img/28.jpg',
    ];

    /** []Texture */
    textures = [];

    /** []MeshBasicMaterial */
    materials = [];

    numberOfSurfaces = this.matrix.elements.length/2;

    radiusForOneSurfaceInDegrees = 360/this.numberOfSurfaces;

    isSpinning = false;

    constructor() {
        "ngInject";
        super();

        this.fillTextures();
        this.fillMaterials();

        const geometry = new CylinderGeometry(2, 2, 2, 32, 1, false);
        this.materials.push(new MeshBasicMaterial());

        this.assignMaterialIndexToFaces(geometry);

        const mesh = new Mesh(geometry, new MeshFaceMaterial(this.materials));
        this.add(mesh);
    }

    increaseXRotation (increaseBy) {
        this.rotation.x += increaseBy;
    }

    getRotationXInDegrees () {
        return this.rotation.x*180/Math.PI;
    }

    getResult () {
        return Math.round(
            this.getRotationXInDegrees()/this.radiusForOneSurfaceInDegrees
            ) % this.numberOfSurfaces;
    }

    assignMaterialIndexToFaces (geometry) {
        let i = 0;
        let j = 0;
        geometry.faces.forEach((face) => {
            if (face.b < 32 || face.c > 64) {
                face.materialIndex = this.imgArray.length;
            } else {
                /** spinner texture fill magic */
                if(i === this.imgArray.length) {
                    i=0;
                    j++;
                }
                i++;
                if(j === this.imgArray.length) {
                    j=0;
                }
                face.materialIndex = j;
            }
        });
    }

    fillMaterials () {
        this.textures.forEach((texture) => {
            let material = this.materialFactory(texture);
            this.materials.push(material);
        });
    }

    fillTextures () {
        this.imgArray.forEach((src) => {
            let texture = this.textureFactory(src);
            this.textures.push(texture);
        });

    }

    textureFactory (src) {
        let texture = new TextureLoader().load(src);
        texture.wrapS = RepeatWrapping;
        texture.wrapT = RepeatWrapping;
        texture.repeat.set(8, 1 );

        return texture;
    }

    materialFactory (texture) {
        return new MeshBasicMaterial({map: texture});
    }
}