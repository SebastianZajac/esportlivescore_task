import Spinner from './spinner.model';

export default class SpinnerFactory {
    make () {
        let spinner = new Spinner();
        spinner.rotateZ(Math.PI / 2);

        return spinner;
    }
};


