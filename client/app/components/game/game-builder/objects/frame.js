import {
    Object3D,
    BoxBufferGeometry,
    MeshBasicMaterial,
    Mesh
} from 'three';

export default class Frame extends Object3D {
    constructor() {
        super();

        const geometry = new BoxBufferGeometry( 7, 5, 3.5 );
        const mesh = new Mesh(geometry, new MeshBasicMaterial());

        this.add(mesh);
    }

}
