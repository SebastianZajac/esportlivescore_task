import {
    WebGLRenderer,
    Scene,
    PerspectiveCamera,
    AmbientLight
} from 'three';
import resize from 'brindille-resize';
import Frame from './objects/frame';

export default class GameBuilderFactory {
    /**  {SpinnerFactory} spinnerFactory */
    spinnerFactory;
    maxSpeedToRandom = 0.05;
    minSpeedToRandom = 0.5;
    speedDecreasementValue = 1.007;
    /**
     * @param {SpinnerFactory} spinnerFactory
     */
    constructor(spinnerFactory) {
        "ngInject";

        this.spinnerFactory = spinnerFactory;
        this.spinners = [];
        this.shiftBetweenSpinners = 2.1;
        this.scene = new Scene();
        this.renderer = new WebGLRenderer();
        this.light = new AmbientLight(0xffffff);
        this.camera = new PerspectiveCamera(70, resize.width / resize.height, 1, 1000);
        this.beforeStop = false;
    }

    initGame(numberOfSlots) {
        this.renderer.setClearColor(0x323232);
        this.renderer.setSize(window.innerWidth/2, window.innerHeight/2);
        document.getElementById('game-window').appendChild(this.renderer.domElement);
        this.scene.add(this.light);

        this.camera.position.set((this.shiftBetweenSpinners * numberOfSlots) - this.shiftBetweenSpinners, 0 , 10);
        this.generateSpinners(numberOfSlots, this.scene);

        this.frame = new Frame();
        this.frame.position.set((this.shiftBetweenSpinners * numberOfSlots) - this.shiftBetweenSpinners, 0, -0.4);
        this.scene.add(this.frame);

        resize.addListener(() => (this.onResize));

        this.render();
    }

    startSpinning() {
        this.beforeStop = true;
        this.rotationSpeedGenerate();
        this.startSpinners();
    }

    stopSpinning(onStopCallback) {
        this.beforeStop = false;
        this.onStopCallback = onStopCallback;
    }

    startSpinners () {
        this.spinners.forEach((spinner) => (
            spinner.item.isSpinning = true
        ));
    }

    onResize() {
        this.camera.aspect = resize.width / resize.height;
        this.camera.updateProjectionMatrix();
        this.renderer.setSize(resize.width/2, resize.height/2);
    }

    generateSpinners(numberOfSlots, scene) {
        let shift = 0;
        for(let i = 0; i < numberOfSlots; i++) {
            let spinner = this.spinnerFactory.make();
            shift = shift+ this.shiftBetweenSpinners;
            spinner.position.x = shift;
            scene.add(spinner);
            this.spinners.push(
                {
                    item: spinner,
                    speed: null
                }
            );
        }
    }

    rotationSpeedGenerate() {
        this.spinners.forEach((spinner) => (
            spinner.speed = this.getRandomArbitrary(this.minSpeedToRandom, this.maxSpeedToRandom)
        ));
    }

    getRandomArbitrary(min, max) {
        return Math.random() * (max - min) + min;
    }

    controlSpinnerRotation(spinner) {
        /** Stop was not activated */
        if(this.beforeStop) {
           return spinner.item.increaseXRotation(spinner.speed);
        }

        /** Spinner stops when surface's border is in place on certain speed*/
        if(
            spinner.item.isSpinning &&
            spinner.speed &&
            spinner.item.getRotationXInDegrees() % spinner.item.radiusForOneSurfaceInDegrees < 0.5 &&
            spinner.speed < 0.015
        ) {
            spinner.item.isSpinning = false;

            if(this.hasGameFinished() && this.onStopCallback) {
                let results = this.getResults();
                this.onStopCallback(results);
                this.resetGameToInitialState();
            }
        /** Spinning and decreasing speed */
        } else if(
            spinner.item.isSpinning && spinner.speed && spinner.speed > 0.001
        ) {
            this.decreaseSpinnerSpeed(spinner)
        }
    }

    resetGameToInitialState () {
        this.onStopCallback = null;
    }

    getResults () {
        let result = {};
        this.spinners.forEach((spinner) => (
            result[spinner.item.getResult()] ? result[spinner.item.getResult()]++ : result[spinner.item.getResult()] = 1
        ));

        return result;
    }

    decreaseSpinnerSpeed (spinner) {
        spinner.speed = spinner.speed / this.speedDecreasementValue;
        return spinner.item.increaseXRotation(spinner.speed);
    }

    hasGameFinished () {
        let temp = true;
        this.spinners.forEach((spinner) => (
            temp = temp && spinner.item.isSpinning === false
        ));

        return temp;
    }

    render() {
        this.spinners.forEach((spinner) => (
                this.controlSpinnerRotation(spinner)
            )
        );

        this.renderer.render(this.scene, this.camera);
        requestAnimationFrame(() => (this.render()))
    }
};


